# Docker-ibdeskPj

Ambiente local dockerizado para levantar apis + front del proyecto IBDesk-PJ

## Instrucciones

En el raiz crear los directorios:

<b>arsenal:</b> Ubicacion de los [repositorios](https://gitlab.santanderbr.corp/wo/ARSENAL) de las api

<b>spa:</b> Ubicacion de los [repositorios](https://gitlab.santanderbr.corp/wo/SPA) del front

Una vez clonado los repositorios dentro de los directorios ejecutar desde el raiz.

```
~$ docker-compose build
```

<i style="color:red">Se realizara la descarga de las dependencias y build de cada uno de los proyectos dentro de sus respectivos contenedores</i>


Una vez finalizado se deben levantar los contenedores mediante el comando

```
~$ docker-compose up
```

Al finalizar se podra acceder mediante <i>localhost:4200</i> al front el cual se encontrara consumiendo los servicios desde las apis dockerizadas

## Implementacion

Mediante el comando docker-compose build o docker-compose up se podra determinar un contenedor especifico en caso que no se requiera levantar todo el proyecto completo.

```
~$ docker-compose build contract-api 
```

```
~$ docker-compose up contract-api 
```

El nombre de cada contenedor se determina dentro del archivo <b>docker-compose.\[servicio].yml</b>